package fr.cnam.foad.nfa035.dao;

import fr.cnam.foad.nfa035.fileutils.streaming.media.ImageByteArrayFrame;
import fr.cnam.foad.nfa035.fileutils.streaming.media.ImageFileFrame;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageSerializerBase64StreamingImpl;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageStreamingSerializer;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
 File wallet;
public class BadgeWalletDAO {
    public BadgeWalletDAO(String filePath) {
        this.wallet = new File(filePath);
    }

    public void addBadge(File image) throws IOException {
        ImageFileFrame media = new ImageFileFrame(wallet);

        // Sérialisation
        ImageStreamingSerializer serializer = new ImageSerializerBase64StreamingImpl();
        serializer.serialize(image, media);
    }
}
